/*Singleton Pattern

  Definition-
  A design pattern that restricts the instantiation of a class to one object.
  Considered Anti Pattern by some people.

  Requirement-
  1. Ensure one instance of class.
  2. Provide global access to that instance.

  Design
  1. Declaring Constructors of that class to be private.
  2. Providing a static method tha returns a reference to the instance.
 */
#include <iostream>
using namespace std;

class singleton {
private:
  singleton() {}
  static singleton *priv_inst;


public:
  static singleton *getInstance() {
    if (!priv_inst) {
      priv_inst = new singleton;
    }
    return priv_inst;
  }
};


void testFunction() {
  // singleton test; - will thrown an error
  singleton *instance = singleton::getInstance();
}
